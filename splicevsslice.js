let colors = ["Red", "Orange", "Yellow", "Green", "Blue", "Purple", "White", "Black", "Gray", "Brown"];

function attachArray() {
    let mainDiv = document.getElementById("main");
    mainDiv.innerHTML = colors;
}

function sliceArray() {
    let startIndex = document.getElementById("sliceStart").value;
    let endIndex = document.getElementById("sliceEnd").value;
    let newArrayDiv = document.getElementById("newArray");
    let slicedColors = colors.slice(startIndex, endIndex);
    newArrayDiv.innerHTML = slicedColors;
}

function removeWithSplice() {
    let startIndex = document.getElementById("spliceStart").value;
    let endIndex = document.getElementById("spliceEnd").value;
    colors.splice(startIndex, endIndex);
    attachArray();
}

function addWithSplice() {
    let startIndex = document.getElementById("spliceStartAdd").value;
    let endIndex = document.getElementById("spliceEndAdd").value;
    let valueToAdd = document.getElementById("spliceAddValue").value;
    colors.splice(startIndex, endIndex, valueToAdd);
    attachArray();
}